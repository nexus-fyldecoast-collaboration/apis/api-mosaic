// @ts-check

const AWS = require("../config/database").AWS;
const tablename = "discharges";
const docClient = new AWS.DynamoDB.DocumentClient();

module.exports.getItemByID = function (index, callback) {
  var params = {
    TableName: tablename,
    KeyConditionExpression: "#index = :index",
    ExpressionAttributeNames: {
      "#index": "index",
    },
    ExpressionAttributeValues: {
      ":index": index,
    },
  };
  docClient.query(params, callback);
};

module.exports.getItemByNHSNumberNumberAndIndex = function (nhsNumber, index, callback) {
  var params = {
    TableName: tablename,
    KeyConditionExpression: "#nhsNumber = :nhsNumber AND #index = :index",
    ExpressionAttributeNames: {
      "#nhsNumber": "nhsNumber",
      "#index": "index",
    },
    ExpressionAttributeValues: {
      ":nhsNumber": nhsNumber,
      ":index": index,
    },
  };
  docClient.query(params, callback);
};

module.exports.getItemsByNHSNumber = function (nhsNumber, callback) {
  var params = {
    TableName: tablename,
    IndexName: "nhsNumber-index",
    KeyConditionExpression: "#nhsNumber = :nhsNumber",
    ExpressionAttributeNames: {
      "#nhsNumber": "nhsNumber",
    },
    ExpressionAttributeValues: {
      ":nhsNumber": nhsNumber,
    },
  };
  docClient.query(params, callback);
};

module.exports.getAll = function (callback) {
  var params = {
    TableName: tablename,
  };
  docClient.scan(params, callback);
};

module.exports.addItem = function (newItem, callback) {
  var client = new AWS.DynamoDB();
  var params = {
    TableName: tablename,
    Item: newItem,
  };
  client.putItem(params, callback);
};

module.exports.removeItem = function (item_index, nhsNumber, callback) {
  var getparams = {
    TableName: tablename,
    Key: {
      index: item_index,
      nhsNumber: nhsNumber,
    },
  };
  console.log(getparams);
  docClient.delete(getparams, (err, result) => {
    if (err) callback(err, { status: 400, msg: err });
    else callback(null, { status: 200, msg: result });
  });
};

module.exports.updateItem = function (updatedItem, callback) {
  const fields = Object.keys(updatedItem);
  const update = "set " + updatefields(fields);
  const expressionvals = updateexpression(fields, updatedItem);
  const expressionnames = updateexpressionnames(fields);
  var params = {
    TableName: tablename,
    Key: {
      index: updatedItem.index,
      nhsNumber: updatedItem.nhsNumber,
    },
    UpdateExpression: update,
    ExpressionAttributeValues: expressionvals,
    ExpressionAttributeNames: expressionnames,
    ReturnValues: "UPDATED_NEW",
  };
  docClient.update(params, callback);
};

module.exports.updateArchive = function (updatedItem, callback) {
  var client = new AWS.DynamoDB();
  var params = {
    TableName: tablename + "_archive",
    Item: updatedItem,
  };
  client.putItem(params, callback);
};

function updatefields(fields) {
  let output = "";
  fields.forEach((val) => {
    output += "#" + val + ":" + val + ",";
  });
  return output.substring(0, output.length - 1);
}

function updateexpression(fields, updateItem) {
  let exp = {};
  fields.forEach((val) => {
    exp[":" + val] = updateItem[val];
  });
  return exp;
}

function updateexpressionnames(fields) {
  let exp = {};
  fields.forEach((val) => {
    exp["#" + val] = val;
  });
  return exp;
}
