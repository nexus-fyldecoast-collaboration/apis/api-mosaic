// @ts-check

const database = require("../models/people");
const crossfilter = require("crossfilter");
let groups = {};
let dimensions = {};
let filters = {};
let model;
let dataset;

module.exports.filterCF = function(filter, callback) {
  callback(getResults(filter), null);
};

module.exports.buildCrossfilter = function() {
  console.log("build...");
  database.getAll(function(err, result) {
    if (err) {
      console.log(err);
      return "Build Failed";
    } else {
      dataset = result;
      model = crossfilter(dataset);
      const all = model.groupAll();

      dimensions.nhs_lad_code = model.dimension(d => d.nhs_lad_code);
      groups.nhs_lad_code = dimensions.nhs_lad_code.group();
      filters.nhs_lad_code = dataMatches;
      dimensions.received_letter = model.dimension(d => d.received_letter);
      groups.received_letter = dimensions.received_letter.group();
      filters.received_letter = dataMatches;
      dimensions.medical_conditions = model.dimension(d => d.medical_conditions);
      groups.medical_conditions = dimensions.medical_conditions.group();
      filters.medical_conditions = dataMatches;
      dimensions.can_you_get_essential_supplies_delivered = model.dimension(
        d => d.can_you_get_essential_supplies_delivered
      );
      groups.can_you_get_essential_supplies_delivered = dimensions.can_you_get_essential_supplies_delivered.group();
      filters.can_you_get_essential_supplies_delivered = dataMatches;
      console.log("Build Finished");
      return "Build Finished";
    }
  });
};

module.exports.updateCrossfilter = function(newItem) {
  model.add(newItem);
};

function getModel() {
  return model;
}
function getGroups() {
  return groups;
}
function getFilters() {
  return filters;
}
function getDimensions() {
  return dimensions;
}

const dataMatches = function(data, filter) {
  return filter.includes(data);
};

const filterContains = function(data, filter) {
  let flag = false;
  data.forEach(filt => {
    if (filter.includes(filt)) {
      flag = true;
    }
  });
  return flag;
};

const dataMatchesFivePlus = function(data, filter) {
  filter.forEach(elem => {
    if (elem.toString().includes("5")) {
      filter.splice(filter.indexOf(elem), 1, "5");
    }
  });
  if (data.includes("5")) {
    data = "5";
  }
  return filter.includes(data);
};

const dataWithinRange = function(data, filter) {
  let flag = false;
  if (filter.length < 2) {
    filter = filter[0];
  }
  const valA = parseInt(filter[0]);
  const valB = parseInt(filter[1]);
  if (data >= valA && data <= valB) flag = true;
  return flag;
};

const getResults = function(filter) {
  var results = {};
  const thisNDX = new NDX();
  thisNDX.init();
  const dims = Object.keys(thisNDX.dimensions);
  dims.forEach(dim => {
    thisNDX.dimensions[dim].filterAll();
  });
  for (let dimension in thisNDX.dimensions) {
    var group = thisNDX.groups[dimension];
    if (filter[dimension]) {
      var filterObj = filter[dimension];
      thisNDX.dimensions[dimension].filterFunction(d => thisNDX.filters[dimension](d, filterObj));
    } else {
      thisNDX.dimensions[dimension].filter(null);
    }
    results[dimension] = {
      values: group.all(),
      top: group.top(1)[0].value,
      filts: filter[dimension]
    };
  }
  results["all"] = {
    values: thisNDX.all.value()
  };
  return results;
};

const NDX = (module.exports.NDX = class NDX {
  constructor() {
    this.groups = {};
    this.dimensions = {};
    this.filters = {};
  }

  init() {
    this.ndx = Object.assign(getModel());
    this.all = this.ndx.groupAll();
    this.dimensions = Object.assign(getDimensions());
    this.groups = Object.assign(getGroups());
    this.filters = Object.assign(getFilters());
    const keys = Object.keys(this.dimensions);
    keys.forEach(key => {
      this.dimensions[key].filterAll();
    });
  }
});
