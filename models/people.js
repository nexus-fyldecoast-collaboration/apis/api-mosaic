const config = require("../config/database");
const Pool = require("pg").Pool;
const pool = new Pool({
  user: config.postgres_un,
  host: config.pgdatabase,
  database: "People",
  password: config.postgres_pw,
  port: config.pgport
});

module.exports.getAll = function(callback) {
  const query = `SELECT * FROM people_sample_datasets`;
  pool.query(query, (error, results) => {
    if (error) {
    }
    if (results && results.rows) {
      callback(null, results.rows);
    } else {
      console.log("Error: " + error);
      callback("No rows returned", null);
    }
  });
};

module.exports.getPersonByNHSNumber = function(nhsnumber, callback) {
  const query = `SELECT * FROM people_sample_datasets WHERE nhs_nhs_number LIKE '` + nhsnumber + `'`;
  pool.query(query, (error, results) => {
    if (error) {
      console.log("Error: " + error);
      callback(error, null);
    }
    if (results && results.rows) {
      callback(null, results.rows);
    } else {
      console.log("Error: " + error);
      callback("No rows returned", null);
    }
  });
};
