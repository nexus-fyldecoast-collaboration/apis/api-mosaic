// @ts-check
const express = require("express");
const router = express.Router();
const passport = require("passport");
const discharges = require("../models/discharges");

/**
 * @swagger
 * tags:
 *   name: Discharges
 *   description: Inpatient Discharge Data
 */

/**
 * @swagger
 * /discharges/register:
 *   post:
 *     security:
 *      - JWT: []
 *     description: Registers or Updates an Item
 *     tags:
 *      - Discharges
 *     produces:
 *      - application/json
 *     parameters:
 *         - in: body
 *           name: discharge
 *           description: The inpatient discharge details.
 *           schema:
 *             type: array
 *             items:
 *                type: object
 *                properties:
 *                  organisation:
 *                     type: string
 *                  visitnumber:
 *                     type: string
 *                  dischargeDT:
 *                     type: string
 *                  dischargeDestination:
 *                     type: string
 *                  hospNo:
 *                    type: string
 *                  nhsNumber:
 *                    type: string
 *                  speciality:
 *                    type: string
 *     responses:
 *       200:
 *         description: Confirmation of Item Registration
 *       400:
 *         description: Bad Request, server doesn't understand input
 *       401:
 *         description: Unauthorised
 *       409:
 *         description: Conflict with something in Database
 *       500:
 *         description: Server Error Processing Result
 */
router.post(
  "/register",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    if (req.body && req.body.length > 0) {
      const item = req.body[0];
      const index = item.organisation + "_" + item.visitnumber;
      discharges.getItemByNHSNumberNumberAndIndex(item.nhsNumber, index, function (err, app) {
        if (err) {
          res.json({
            success: false,
            msg: "Failed to update: " + err,
          });
        }
        if (app.Items.length > 0) {
          var scannedItem = app.Items[0];
          const date = new Date();
          const archiveindex =
            scannedItem.index +
            "_" +
            date.toISOString().slice(0, 19).replace("T", "").replace(/:/g, "").replace(/-/g, "");
          let archiveItem = {
            index: { S: archiveindex },
            nhsNumber: { S: scannedItem.nhsNumber },
          };

          if (scannedItem.organisation) archiveItem.organisation = { S: scannedItem.organisation };
          if (scannedItem.visitnumber) archiveItem.visitnumber = { S: scannedItem.visitnumber };
          if (scannedItem.hospNo) archiveItem.hospNo = { S: scannedItem.hospNo };
          if (scannedItem.dischargeDT) archiveItem.dischargeDT = { S: convertDateTime(scannedItem.dischargeDT) };
          if (scannedItem.dischargeDestination)
            archiveItem.dischargeDestination = { S: scannedItem.dischargeDestination };
          if (scannedItem.speciality) archiveItem.speciality = { S: scannedItem.speciality };
          discharges.updateArchive(archiveItem, function (error, data) {
            if (error) {
              console.log("Unable to archive old discharge: " + error);
            }
          });

          if (item.organisation) scannedItem.organisation = item.organisation;
          if (item.visitnumber) scannedItem.visitnumber = item.visitnumber;
          if (item.hospNo) scannedItem.hospNo = item.hospNo;
          if (item.dischargeDT) scannedItem.dischargeDT = convertDateTime(item.dischargeDT);
          if (item.dischargeDestination) scannedItem.dischargeDestination = item.dischargeDestination;
          if (item.speciality) scannedItem.speciality = item.speciality;

          discharges.updateItem(scannedItem, function (err, data) {
            if (err) {
              res.json({
                success: false,
                msg: "Failed to update: " + err,
              });
            }
            res.json({
              success: true,
              msg: "Item updated",
            });
          });
        } else {
          let newItem = {
            index: { S: index },
            nhsNumber: { S: item.nhsNumber },
          };

          if (item.organisation) newItem.organisation = { S: item.organisation };
          if (item.visitnumber) newItem.visitnumber = { S: item.visitnumber };
          if (item.hospNo) newItem.hospNo = { S: item.hospNo };
          if (item.dischargeDestination) newItem.dischargeDestination = { S: item.dischargeDestination };
          if (item.dischargeDT) newItem.dischargeDT = { S: convertDateTime(item.dischargeDT) };
          if (item.speciality) newItem.speciality = { S: item.speciality };

          discharges.addItem(newItem, (err, user) => {
            if (err) {
              res.status(400).json({
                success: false,
                msg: "Failed to register: " + err,
              });
            } else {
              res.status(200).json({
                success: true,
                msg: "Registered",
              });
            }
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: "Incorrect format of message",
      });
    }
  }
);

/**
 * @swagger
 * /discharges/remove:
 *   post:
 *     security:
 *      - JWT: []
 *     description: Removes the Item
 *     tags:
 *      - Discharges
 *     produces:
 *      - application/json
 *     parameters:
 *       - name: index
 *         description: Item Index
 *         in: formData
 *         required: true
 *         type: string
 *       - name: nhsNumber
 *         description: Patient NHS Number
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Full List
 *       400:
 *         description: Bad Request, server doesn't understand input
 *       401:
 *         description: Unauthorised
 */
router.post(
  "/remove",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    const index = req.body.index;
    const nhsNumber = req.body.nhsNumber;
    discharges.removeItem(index, nhsNumber, function (err, result) {
      if (err) {
        res.json(result);
      } else {
        res.json({
          success: true,
          msg: "Item removed",
        });
      }
    });
  }
);

/**
 * @swagger
 * /discharges/getAll:
 *   get:
 *     security:
 *      - JWT: []
 *     description: Returns the entire collection
 *     tags:
 *      - Discharges
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Full List
 *       401:
 *         description: Unauthorised
 */
router.get(
  "/getAll",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    discharges.getAll(function (err, result) {
      if (err) {
        res.send(err);
      } else {
        if (result.Items) {
          res.send(JSON.stringify(result.Items));
        } else {
          res.send("[]");
        }
      }
    });
  }
);

/**
 * @swagger
 * /discharges/getByIndex?index={index}:
 *   get:
 *     security:
 *      - JWT: []
 *     description: Returns the entire collection
 *     tags:
 *      - Discharges
 *     produces:
 *      - application/json
 *     parameters:
 *       - name: index
 *         description: Test Index
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Full List
 *       401:
 *         description: Unauthorised
 */
router.get(
  "/getByIndex",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    const index = req.url.replace("/getByIndex?index=", "");
    discharges.getItemByID(index, function (err, result) {
      if (err) {
        res.send(err);
      } else {
        if (result.Items) {
          res.send(JSON.stringify(result.Items));
        } else {
          res.send("[]");
        }
      }
    });
  }
);

/**
 * @swagger
 * /discharges/getItemsByNHSNumber?nhsNumber={nhsNumber}:
 *   get:
 *     security:
 *      - JWT: []
 *     description: Returns the entire collection
 *     tags:
 *      - Discharges
 *     produces:
 *      - application/json
 *     parameters:
 *       - name: nhsNumber
 *         description: Patient's NHS Number
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Full List
 *       401:
 *         description: Unauthorised
 */
router.get(
  "/getItemsByNHSNumber",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    const nhsNumber = req.url.replace("/getItemsByNHSNumber?nhsNumber=", "");
    discharges.getItemsByNHSNumber(nhsNumber, function (err, result) {
      if (err) {
        res.send(err);
      } else {
        if (result.Items) {
          res.send(JSON.stringify(result.Items));
        } else {
          res.send("[]");
        }
      }
    });
  }
);

module.exports = router;

function convertDateTime(sent) {
  const year = sent.substr(0, 4);
  const month = sent.substr(3, 2);
  const day = sent.substr(5, 2);
  const hours = sent.substr(7, 2);
  const mins = sent.substr(9, 2);
  const secs = sent.substr(11, 2);
  const response = new Date(
    parseInt(year),
    parseInt(month),
    parseInt(day),
    parseInt(hours),
    parseInt(mins),
    parseInt(secs)
  );
  return response.toISOString();
}
