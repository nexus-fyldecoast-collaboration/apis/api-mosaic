// @ts-check
const express = require("express");
const router = express.Router();
const passport = require("passport");
const labtests = require("../models/labtests");

/**
 * @swagger
 * tags:
 *   name: LabTests
 *   description: Lab Tests for COVID Data
 */

/**
 * @swagger
 * /labtests/register:
 *   post:
 *     security:
 *      - JWT: []
 *     description: Registers or Updates an Item
 *     tags:
 *      - LabTests
 *     produces:
 *      - application/json
 *     parameters:
 *         - in: body
 *           name: labtest
 *           description: The lab test details.
 *           schema:
 *             type: array
 *             items:
 *                type: object
 *                properties:
 *                  SendingFacility:
 *                     type: string
 *                  MessageControlID:
 *                     type: string
 *                  hospNo:
 *                    type: string
 *                  nhsNumber:
 *                    type: string
 *                  AssignedPatientLocation:
 *                    type: string
 *                  FillerOrderNumber:
 *                    type: string
 *                  ObservationDateTime:
 *                    type: string
 *                  SpecimenRecievedDateTime:
 *                    type: string
 *                  ResultsRptStatusChngDateTime:
 *                    type: string
 *                  ObservationIdentifieridentifierST:
 *                    type: string
 *                  ObservationValue:
 *                    type: string
 *     responses:
 *       200:
 *         description: Confirmation of Item Registration
 *       400:
 *         description: Bad Request, server doesn't understand input
 *       401:
 *         description: Unauthorised
 *       409:
 *         description: Conflict with something in Database
 *       500:
 *         description: Server Error Processing Result
 */
router.post(
  "/register",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    if (req.body && req.body.length > 0) {
      const item = req.body[0];
      const index = item.SendingFacility + "_" + item.AssignedPatientLocation;
      labtests.getItemByFillerOrderNumberAndLocation(item.FillerOrderNumber, index, function (err, app) {
        if (err) {
          res.json({
            success: false,
            msg: "Failed to update: " + err,
          });
        }
        if (app.Items.length > 0) {
          var scannedItem = app.Items[0];
          const date = new Date();
          const archiveindex =
            scannedItem.index +
            "_" +
            date.toISOString().slice(0, 19).replace("T", "").replace(/:/g, "").replace(/-/g, "");
          let archiveItem = {
            index: { S: archiveindex },
            FillerOrderNumber: { S: scannedItem.FillerOrderNumber },
          };

          if (scannedItem.SendingFacility) archiveItem.SendingFacility = { S: scannedItem.SendingFacility };
          if (scannedItem.MessageControlID) archiveItem.MessageControlID = { S: scannedItem.MessageControlID };
          if (scannedItem.hospNo) archiveItem.hospNo = { S: scannedItem.hospNo };
          if (scannedItem.nhsNumber) archiveItem.nhsNumber = { S: scannedItem.nhsNumber };
          if (scannedItem.AssignedPatientLocation)
            archiveItem.AssignedPatientLocation = { S: scannedItem.AssignedPatientLocation };
          if (scannedItem.ObservationDateTime) archiveItem.ObservationDateTime = { S: scannedItem.ObservationDateTime };
          if (scannedItem.SpecimenRecievedDateTime)
            archiveItem.SpecimenRecievedDateTime = { S: scannedItem.SpecimenRecievedDateTime };
          if (scannedItem.ResultsRptStatusChngDateTime)
            archiveItem.ResultsRptStatusChngDateTime = { S: scannedItem.ResultsRptStatusChngDateTime };
          if (scannedItem.ObservationIdentifieridentifierST)
            archiveItem.ObservationIdentifieridentifierST = { S: scannedItem.ObservationIdentifieridentifierST };
          if (scannedItem.ObservationValue) archiveItem.ObservationValue = { S: scannedItem.ObservationValue };
          labtests.updateArchive(archiveItem, function (error, data) {
            if (error) {
              console.log("Unable to archive old labtest: " + error);
            }
          });

          if (item.SendingFacility) scannedItem.SendingFacility = item.SendingFacility;
          if (item.MessageControlID) scannedItem.MessageControlID = item.MessageControlID;
          if (item.hospNo) scannedItem.hospNo = item.hospNo;
          if (item.nhsNumber) scannedItem.nhsNumber = item.nhsNumber;
          if (item.AssignedPatientLocation) scannedItem.AssignedPatientLocation = item.AssignedPatientLocation;
          if (item.ObservationDateTime) scannedItem.ObservationDateTime = convertDateTime(item.ObservationDateTime);
          if (item.SpecimenRecievedDateTime)
            scannedItem.SpecimenRecievedDateTime = convertDateTime(item.SpecimenRecievedDateTime);
          if (item.ResultsRptStatusChngDateTime)
            scannedItem.ResultsRptStatusChngDateTime = convertDateTime(item.ResultsRptStatusChngDateTime);
          if (item.ObservationIdentifieridentifierST)
            scannedItem.ObservationIdentifieridentifierST = item.ObservationIdentifieridentifierST;
          if (item.ObservationValue) scannedItem.ObservationValue = item.ObservationValue;

          labtests.updateItem(scannedItem, function (err, data) {
            if (err) {
              res.json({
                success: false,
                msg: "Failed to update: " + err,
              });
            }
            res.json({
              success: true,
              msg: "Item updated",
            });
          });
        } else {
          let newItem = {
            index: { S: index },
            FillerOrderNumber: { S: item.FillerOrderNumber },
          };

          if (item.SendingFacility) newItem.SendingFacility = { S: item.SendingFacility };
          if (item.MessageControlID) newItem.MessageControlID = { S: item.MessageControlID };
          if (item.hospNo) newItem.hospNo = { S: item.hospNo };
          if (item.nhsNumber) newItem.nhsNumber = { S: item.nhsNumber };
          if (item.AssignedPatientLocation) newItem.AssignedPatientLocation = { S: item.AssignedPatientLocation };
          if (item.ObservationDateTime) newItem.ObservationDateTime = { S: convertDateTime(item.ObservationDateTime) };
          if (item.SpecimenRecievedDateTime)
            newItem.SpecimenRecievedDateTime = { S: convertDateTime(item.SpecimenRecievedDateTime) };
          if (item.ResultsRptStatusChngDateTime)
            newItem.ResultsRptStatusChngDateTime = { S: convertDateTime(item.ResultsRptStatusChngDateTime) };
          if (item.ObservationIdentifieridentifierST)
            newItem.ObservationIdentifieridentifierST = { S: item.ObservationIdentifieridentifierST };
          if (item.ObservationValue) newItem.ObservationValue = { S: item.ObservationValue };

          labtests.addItem(newItem, (err, user) => {
            if (err) {
              res.status(400).json({
                success: false,
                msg: "Failed to register: " + err,
              });
            } else {
              res.status(200).json({
                success: true,
                msg: "Registered",
              });
            }
          });
        }
      });
    } else {
      res.json({
        success: false,
        msg: "Incorrect format of message",
      });
    }
  }
);

/**
 * @swagger
 * /labtests/remove:
 *   post:
 *     security:
 *      - JWT: []
 *     description: Removes the Item
 *     tags:
 *      - LabTests
 *     produces:
 *      - application/json
 *     parameters:
 *         - in: body
 *           name: lab test
 *           description: The lab test details.
 *           schema:
 *                type: object
 *                properties:
 *                  index:
 *                     type: string
 *                  FillerOrderNumber:
 *                     type: string
 *     responses:
 *       200:
 *         description: Full List
 *       400:
 *         description: Bad Request, server doesn't understand input
 *       401:
 *         description: Unauthorised
 */
router.post(
  "/remove",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    const index = req.body.index;
    const FillerOrderNumber = req.body.FillerOrderNumber;
    labtests.removeItem(index, FillerOrderNumber, function (err, result) {
      if (err) {
        res.json(result);
      } else {
        res.json({
          success: true,
          msg: "Item removed",
        });
      }
    });
  }
);

/**
 * @swagger
 * /labtests/getAll:
 *   get:
 *     security:
 *      - JWT: []
 *     description: Returns the entire collection
 *     tags:
 *      - LabTests
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Full List
 *       401:
 *         description: Unauthorised
 */
router.get(
  "/getAll",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    labtests.getAll(function (err, result) {
      if (err) {
        res.send(err);
      } else {
        if (result.Items) {
          res.send(JSON.stringify(result.Items));
        } else {
          res.send("[]");
        }
      }
    });
  }
);

/**
 * @swagger
 * /labtests/getByIndex?index={index}:
 *   get:
 *     security:
 *      - JWT: []
 *     description: Returns the entire collection
 *     tags:
 *      - LabTests
 *     produces:
 *      - application/json
 *     parameters:
 *       - name: index
 *         description: Test Index
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Full List
 *       401:
 *         description: Unauthorised
 */
router.get(
  "/getByIndex",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    const index = req.url.replace("/getByIndex?index=", "");
    labtests.getItemByID(index, function (err, result) {
      if (err) {
        res.send(err);
      } else {
        if (result.Items) {
          res.send(JSON.stringify(result.Items));
        } else {
          res.send("[]");
        }
      }
    });
  }
);

/**
 * @swagger
 * /labtests/getItemsByNHSNumber?nhsNumber={nhsNumber}:
 *   get:
 *     security:
 *      - JWT: []
 *     description: Returns the entire collection
 *     tags:
 *      - LabTests
 *     produces:
 *      - application/json
 *     parameters:
 *       - name: nhsNumber
 *         description: Patient's NHS Number
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Full List
 *       401:
 *         description: Unauthorised
 */
router.get(
  "/getItemsByNHSNumber",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    const nhsNumber = req.url.replace("/getItemsByNHSNumber?nhsNumber=", "");
    labtests.getItemsByNHSNumber(nhsNumber, function (err, result) {
      if (err) {
        res.send(err);
      } else {
        if (result.Items) {
          res.send(JSON.stringify(result.Items));
        } else {
          res.send("[]");
        }
      }
    });
  }
);

/**
 * @swagger
 * /labtests/getItemsByFillerOrderNumber?FillerOrderNumber={FillerOrderNumber}:
 *   get:
 *     security:
 *      - JWT: []
 *     description: Returns the entire collection
 *     tags:
 *      - LabTests
 *     produces:
 *      - application/json
 *     parameters:
 *       - name: FillerOrderNumber
 *         description: Lab Test's FillerOrderNumber
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Full List
 *       401:
 *         description: Unauthorised
 */
router.get(
  "/getItemsByFillerOrderNumber",
  passport.authenticate("jwt", {
    session: false,
  }),
  (req, res, next) => {
    const FillerOrderNumber = req.url.replace("/getItemsByFillerOrderNumber?FillerOrderNumber=", "");
    labtests.getItemByFillerOrderNumber(FillerOrderNumber, function (err, result) {
      if (err) {
        res.send(err);
      } else {
        if (result.Items) {
          res.send(JSON.stringify(result.Items));
        } else {
          res.send("[]");
        }
      }
    });
  }
);

module.exports = router;

function convertDateTime(sent) {
  const year = sent.substr(0, 4);
  const month = sent.substr(3, 2);
  const day = sent.substr(5, 2);
  const hours = sent.substr(7, 2);
  const mins = sent.substr(9, 2);
  const secs = sent.substr(11, 2);
  const response = new Date(
    parseInt(year),
    parseInt(month),
    parseInt(day),
    parseInt(hours),
    parseInt(mins),
    parseInt(secs)
  );
  return response.toISOString();
}
