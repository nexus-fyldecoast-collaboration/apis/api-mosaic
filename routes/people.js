// // @ts-check
// const express = require("express");
// const router = express.Router();
// const passport = require("passport");
// const database = require("../models/people");
// const NDXfilter = require("../models/crossfilter").filterCF;

// /**
//  * @swagger
//  * tags:
//  *   name: People
//  *   description: Providing information on recorded People
//  */

// /**
//  * @swagger
//  *  /people/getAll:
//  *   get:
//  *     security:
//  *      - JWT: []
//  *     description: Returns the entire collection
//  *     tags:
//  *      - People
//  *     produces:
//  *      - application/json
//  *     responses:
//  *       200:
//  *         description: Full List
//  */
// router.get(
//   "/getAll",
//   passport.authenticate("jwt", {
//     session: false
//   }),
//   (req, res, next) => {
//     // add role filter here
//     database.getAll(function(err, result) {
//       if (err) {
//         res.send(err);
//       } else {
//         if (result) {
//           res.send(JSON.stringify(result));
//         } else {
//           res.send("[]");
//         }
//       }
//     });
//   }
// );

// /**
//  * @swagger
//  * /people/getByNHSNumber?nhsnumber={nhsnumber}:
//  *   get:
//  *     security:
//  *      - JWT: []
//  *     description: Returns the entire collection
//  *     tags:
//  *      - People
//  *     produces:
//  *      - application/json
//  *     parameters:
//  *       - name: nhsnumber
//  *         description: NHS Number
//  *         in: query
//  *         required: true
//  *         type: string
//  *     responses:
//  *       200:
//  *         description: Full List
//  */
// router.get(
//   "/getByNHSNumber",
//   passport.authenticate("jwt", {
//     session: false
//   }),
//   (req, res, next) => {
//     // add role check here
//     const id = req.url.replace("/getByNHSNumber?nhsnumber=", "");
//     database.getPersonByNHSNumber(id, function(err, result) {
//       if (err) {
//         res.send(err);
//       } else {
//         if (result) {
//           res.send(JSON.stringify(result));
//         } else {
//           res.send(JSON.stringify([]));
//         }
//       }
//     });
//   }
// );

// /**
//  * @swagger
//  * /people/getCrossfilter:
//  *   get:
//  *     security:
//  *      - JWT: []
//  *     description: Queries the Server Side Crossfilter Object
//  *     tags:
//  *      - People
//  *     produces:
//  *      - application/json
//  *     responses:
//  *       200:
//  *         description: Full List
//  */
// router.get(
//   "/getCrossfilter",
//   passport.authenticate("jwt", {
//     session: false
//   }),
//   (req, res, next) => {
//     const filter = req.param("filter") ? JSON.parse(req.param("filter")) : {};
//     NDXfilter(filter, function(cf, err) {
//       res.send(cf);
//     });
//   }
// );

// module.exports = router;
