// @ts-check

// SETUP
// =============================================================================
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const app = express();
const morgan = require("morgan");
const winston = require("winston");
const expressWinston = require("express-winston");
const CloudWatchTransport = require("winston-aws-cloudwatch");
const jwt = require("jsonwebtoken");

// GLOBAL VARIABLES
const port = process.env.PORT || 8091;
// const people = require("./routes/people");
const labtests = require("./routes/labtests");
const admissions = require("./routes/admissions");
const discharges = require("./routes/discharges");
const mortality = require("./routes/mortality");

// SWAGGER SETUP
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const swaggerDefinition = {
  basePath: "/",
  securityDefinitions: {
    JWT: {
      type: "apiKey",
      name: "Authorization",
      in: "header",
    },
  },
};
const options = {
  swaggerDefinition,
  apis: ["./routes/*.js"],
};
const swaggerSpec = swaggerJSDoc(options);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// ERROR LOGGER SETUP
morgan.token("token", function (req) {
  if (req.headers.authorization) {
    return JSON.stringify(jwt.decode(req.headers.authorization.replace("JWT ", "")));
  } else {
    return "{}";
  }
});
// const winstonOptions = {
//   db: config.database + database,
//   label: process.env.API_NAME || "API",
//   metaKey: "metaData"
// };
const addAppNameFormat = winston.format((info) => {
  info.appName = "API";
  return info;
});
const parseMessage = function (message) {
  const variables = message.split("|");
  const token = JSON.parse(variables[6]);
  let username = "";
  if (token && token.username) username = token.username;
  let organisation = "";
  if (token && token.organisation) organisation = token.organisation;
  return {
    method: variables[0],
    url: variables[1],
    status: variables[2],
    response: variables[3],
    responsetime: variables[4],
    date: variables[5],
    token: {
      username: username,
      organisation: organisation,
    },
  };
};
const apiname = process.env.API_NAME || "API";
const AWSSettings = require("./config/database");
const awstransport = new CloudWatchTransport({
  logGroupName: "api-calls",
  logStreamName: apiname,
  createLogGroup: false,
  createLogStream: true,
  submissionInterval: 2000,
  submissionRetryCount: 1,
  batchSize: 1,
  awsConfig: {
    accessKeyId: AWSSettings.accessKeyId,
    secretAccessKey: AWSSettings.secretAccessKey,
    region: AWSSettings.awsregion,
  },
  formatLog: (item) => `${item.level}: ${item.message} ${JSON.stringify(item.meta)}`,
});
const logger = new winston.createLogger({
    // transports: [new winston.transports.MongoDB(winstonOptions)],
    transports: [awstransport],
    format: winston.format.combine(addAppNameFormat(), winston.format.json()),
    dynamicMeta: function (req, res, err) {
      return req.header;
    },
    meta: true,
    expressFormat: true,
    colorize: false,
  }),
  loggerstream = {
    write: function (message, encoding) {
      logger.info(message.replace(/\n$/, ""), {
        metaData: parseMessage(message.replace(/\n$/, "")),
      });
    },
  };
app.use(
  morgan(":method|:url|:status|:res[content-length]|:response-time|:date[iso]|:token", {
    stream: loggerstream,
  })
);

// SETTINGS FOR OUR API
// =============================================================================
app.use(cors());
app.use(express.static(path.join(__dirname, "public")));
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

// app.use("/people", people);
app.use("/labtests", labtests);
app.use("/admissions", admissions);
app.use("/discharges", discharges);
app.use("/mortality", mortality);
app.use(passport.initialize());
app.use(passport.session());
require("./config/passport")(passport);

// ROUTES FOR OUR API
// =============================================================================
app.get("/", (req, res) => {
  res.send("Invalid endpoint");
});

// BUILD CROSSFILTER OBJECT ON LOAD
// =============================================================================
// const crossfilter = require("./models/crossfilter");
// const initbuild = crossfilter.buildCrossfilter();

// START THE SERVER
// =============================================================================
app.listen(port);
console.log("RESTful API now Live on Port: " + port);
